# Technical challenge

Just do `docker-compose up -d` and open the app in the browser with this address `http://localhost:3000`

## Basic Idea

Displaying a unionized layer of hotspots in the beginning and then allowing users to add filters and show particular hotspots as they need. The data is too big to do any on-the-fly calculations (such as buffer or merge) on my system. 
