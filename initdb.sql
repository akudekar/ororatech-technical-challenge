CREATE EXTENSION IF NOT EXISTS postgis;

CREATE TABLE public.forest_fires
(
    lon double precision,
    lat double precision,
    radius double precision,
    tz timestamp without time zone,
    satellite character varying
);

ALTER TABLE IF EXISTS public.forest_fires
    OWNER to postgres;

\COPY public.forest_fires (lon, lat, radius, tz, satellite) FROM '/var/lib/postgresql/data/input-data.csv' DELIMITER ',' CSV HEADER ENCODING 'UTF8';

ALTER TABLE IF EXISTS public.forest_fires
    ADD COLUMN geom geometry;

ALTER TABLE IF EXISTS public.forest_fires
    ADD COLUMN id serial NOT NULL;
ALTER TABLE IF EXISTS public.forest_fires
    ADD PRIMARY KEY (id);

UPDATE public.forest_fires SET geom = ST_SetSRID(ST_MakePoint(forest_fires.lon, forest_fires.lat), 4326);

create table public.union as SELECT st_union(ST_Buffer(geom, 0.03)) FROM public.forest_fires