import React, { useEffect, useRef, useState } from 'react';
import 'ol/ol.css';
import Map from 'ol/Map';
import View from 'ol/View';
import TileLayer from 'ol/layer/Tile';
import OSM from 'ol/source/OSM';
import VectorLayer from 'ol/layer/Vector';
import VectorSource from 'ol/source/Vector';
import GeoJSON from 'ol/format/GeoJSON';
import Select from 'ol/interaction/Select';
import Draw from 'ol/interaction/Draw';
import Overlay from 'ol/Overlay';
import { Feature } from 'ol';
import { Point } from 'ol/geom';
import { DatePicker } from 'antd';
import { Button, Space, Spin, PageHeader } from 'antd';
import { Circle as CircleStyle, Fill, Stroke, Style } from 'ol/style';
import { containsExtent } from 'ol/extent';

function App() {
  const [map, setMap] = useState(null);
  const [timeString, setTimeString] = useState(null);
  const [show, setShow] = useState(false);  
  const mapRef = useRef(null);
  const [extent, setExtent] = useState(null);
  const { RangePicker } = DatePicker;;

  // Defining styles for layers
  const selectstyle = new Style({
    image: new CircleStyle({
      radius: 5,
      fill: new Fill({
        color: 'rgba(0, 255, 255, 0.5)',
      }),
      stroke: new Stroke({
        color: 'rgba(255, 0, 0, 1)',
        width: 0.5,
      }),
    }),
  });

  const normalstyle = new Style({
    image: new CircleStyle({
      radius: 5,
      fill: new Fill({
        color: 'rgba(0, 0, 0, 0.5)',
      }),
      stroke: new Stroke({
        color: 'rgba(255, 0, 0, 0.5)',
        width: 0.5,
      }),
    }),
  });

  const polystyle = new Style({
    fill: new Fill({
      color: 'rgba(255, 0, 0, 0.5)',
    }),
    stroke: new Stroke({
      color: 'rgba(255, 0, 0, 1)',
      width: 2,
    })
  });

  const polybackgroundstyle = new Style({
    fill: new Fill({
      color: 'rgba(255, 0, 0, 0.1)',
    }),
    stroke: new Stroke({
      color: 'rgba(255, 0, 0, 1)',
      width: 0.5,
    })
  });

  // Defining layers

  const vectorSource = new VectorSource({
    url: 'http://localhost:5000/data/union',
    format: new GeoJSON(),
  });
  
  const vectorLayer = new VectorLayer({
    source: vectorSource,
  });

  var layerextent;
  var popup = null;

  useEffect(() => {
    const initialMap = new Map({
      target: mapRef.current,
      layers: [
        new TileLayer({
          source: new OSM()
        }),
        vectorLayer
      ],
      view: new View({
        center: [0, 0],
        zoom: 2,
        projection: 'EPSG:4326'
      })
    });

    // Zooming to layer extent

    vectorSource.once('change', function() { 
      if (vectorSource.getState() === 'ready') {
        initialMap.getView().fit(vectorSource.getExtent());
        initialMap.getView().setZoom(initialMap.getView().getZoom() - 0.3);
        vectorLayer.setStyle(polystyle);
        layerextent = vectorSource.getExtent();
        console.log(extent);
        setExtent(layerextent)
      }
    });

    // Select interaction for individual hostpots with a OL overlay popup
    const select = new Select();
    select.on('select', function(evt) {
      if (initialMap.getLayers().getArray().find(layer => layer.get('name') == 'filteredfeatures')) {
      if (evt.selected.length > 0) {
        if (popup) {
          initialMap.removeOverlay(popup);
        }
        const feature = evt.selected[0];
        const properties = feature.getProperties();
        evt.selected[0].setStyle(selectstyle);
        vectorLayer.setStyle(polybackgroundstyle);
        const content = document.createElement('div');
        content.style.backgroundColor = 'white';
        content.style.padding = '5px';
        content.style.borderRadius = '10px';
        content.style.boxShadow = '0 1px 15px rgba(0,0,0,0.8)';
        content.style.border = '1px solid rgba(0,0,0,0.2)';
        content.style.fontFamily = 'sans-serif';
        const closer = document.createElement('button');
        closer.innerText= 'X'
        closer.style.float = 'right';
        closer.style.borderRadius = '25px';
        closer.style.backgroundColor = 'red';
        closer.style.color = 'white';
        closer.style.margin = '5px';
        closer.onclick = function() {
          initialMap.removeOverlay(popup);
          evt.selected[0].setStyle(normalstyle);
          vectorLayer.setStyle(polystyle);
          closer.blur();
        };
        content.appendChild(closer);
        const table = document.createElement('table');
        table.className = 'ol-popup-table';
        for (const [key, value] of Object.entries(properties)) {
          if (key !== 'geometry') {
            const row = document.createElement('tr');
            const label = document.createElement('td');
            label.className = 'ol-popup-label';
            label.innerText = key;
            const text = document.createElement('td');
            text.className = 'ol-popup-text';
            text.innerText = value;
            row.appendChild(label);
            row.appendChild(text);
            table.appendChild(row);
          }
        }
        content.appendChild(table);
        popup = new Overlay({
          element: content,
          position: evt.mapBrowserEvent.coordinate,
          offset: [0, -20],
          positioning: 'bottom-center',
          stopEvent: false
        });
        initialMap.addOverlay(popup);
      }
    }
    });

      initialMap.addInteraction(select);
    
    initialMap.on('click', function(evt) {
      if (popup) {
        initialMap.removeOverlay(popup);
        vectorLayer.setStyle(polystyle);
      }
    });
    setMap(initialMap);
  }, []);

  let filteredlayer;

  // Filtering based on time range

  const refreshMap = () => {
  console.log(map.getView().calculateExtent());
  console.log(extent);
  if (containsExtent(map.getView().calculateExtent(), extent)) {
    console.log('in extent');
    var data = sessionStorage.getItem('time');
    var data2 = JSON.parse(data);
    var array = data2.time;
    const startTime = array[0];
    const endTime = array[1];
    if (startTime && endTime) {
      const url = `http://localhost:5000/data/filtered?start_time=${startTime}&end_time=${endTime}`;
      fetch(url)
        .then(response => response.json())
        .then(data => {
          const features = new GeoJSON().readFeatures(data);
          const vectorSourcep = new VectorSource();
          vectorSourcep.addFeatures(features); // Add the filtered features to the source
          var filterbutton = document.getElementById('filter-button');
          filterbutton.innerText = 'Filter (' + vectorSourcep.getFeatures().length + ' hotspots found)';
          if (filteredlayer) {
            map.removeLayer(filteredlayer); // Remove the previous filtered layer from the map
          }
          filteredlayer = new VectorLayer({
            source: vectorSourcep,
            style: normalstyle,
            name: 'filteredfeatures'
          });
          map.addLayer(filteredlayer);
          map.getView().fit(vectorSourcep.getExtent());
          map.getView().setZoom(map.getView().getZoom()); // Add the new filtered layer to the map
          console.log('added');
        })
        .catch(error => console.error(error));
      }
    }
  }

  // Clear filter

  const clear_filter = () => {
    var full = map.getLayers().getArray().find(layer => layer.get('name') == 'filteredfeatures');
    if (full) {
      map.removeLayer(full);
      var filterbutton = document.getElementById('filter-button');
      filterbutton.innerText = 'Filter';
    }
  }

  // Manually draw a hotspot

  const drawsource = new VectorSource();
  const drawlayer = new VectorLayer({source: drawsource, name: 'drawlayer'});
  let draw;
  let newfeature;

  const drawhotspot = () => {
    draw = new Draw({
      source: drawsource,
      type: 'Point',
    });
    map.addInteraction(draw);
    draw.on('drawend', function(evt) {
      console.log(evt.feature.getGeometry().getCoordinates());
      newfeature = evt.feature.getGeometry().getCoordinates();
    });
    if (map.getLayers().getArray().find(layer => layer.get('name') == 'drawlayer')) {
      drawsource.addFeature(new Feature(new Point(newfeature)))
    }
    else {
      map.addLayer(drawlayer);
    }
    drawlayer.setStyle(normalstyle);
  }

  // Save the manually drawn hotspot
  const savenewfeature = () => {
    if (newfeature) {
      const url = `http://localhost:5000/data/insert?lon=${newfeature[0]}&lat=${newfeature[1]}`;
      fetch(url)
        .then(response => response.json())
        .then(data => {
          console.log(data);
          alert('New hotspot added!');
        })
        .catch(error => console.error(error));
      map.removeInteraction(draw);  
    }
  }

  // Show all hotspots from the entire table

  const showallfeatures = () => {
    
    const vectorSourcea = new VectorSource({
      url: 'http://localhost:5000/data',
      format: new GeoJSON(),
    })
    const vectorLayera = new VectorLayer({
      source: vectorSourcea,
      name: 'allfeatures',
      style: new Style({
        image: new CircleStyle({
          radius: 3,
          fill: new Fill({
            color: 'rgba(255, 0, 0, 0.5)',
          }),
          stroke: new Stroke({
            color: 'rgba(255, 0, 0, 1)',
            width: 0.5,
          }),
        }),
      }),
    });
    if (containsExtent(map.getView().calculateExtent(), extent)) {
      if (show == false) { 
      map.addLayer(vectorLayera);
      vectorLayera.setStyle(normalstyle)
      setShow(true);
      var showbutton = document.getElementById('show-button');
      showbutton.innerText = 'Loading...';
      vectorSourcea.on('change', function(e) {
        if (vectorSourcea.getState() === 'ready') {
          showbutton.innerText = 'Hide all hotspots';
        }
      });
      }
      if (show == true) {
        var full = map.getLayers().getArray().find(layer => layer.get('name') == 'allfeatures');
        console.log(full);
        full.getSource().clear();
        map.removeLayer(full);
        setShow(false);
        var showbutton = document.getElementById('show-button');
        showbutton.innerText = 'Show all hotspots';
      }
    }
  }

  return (
      <div style={{ position: 'absolute', height: '100%', width: '100%'}}>
        <div class="header" style={{margin: '0px', padding: '0px', height: '50px', width: '100%'}}>
        <h2 style={{position: 'relative', marginLeft: '700px'}}>Ororatech technical challenge - forest fires app</h2>
        </div>
      <div ref={mapRef} style={{ height: '800px', width: '100%' }} />
      <div style={{ position: 'absolute', top: 10, right: 10, zIndex: 1 }}>
        <div style={{backgroundColor: 'white', padding:'20px', borderRadius:'8px', marginTop: '70px'}}>
        <p>Please select a timerange to view individual hotspots.</p>
        <RangePicker
          size='large'
          showTime={{ format: 'HH:mm' }}
          format="YYYY-MM-DD HH:mm"
          onOk={(time) => {
            setTimeString(timeString);
            var data = JSON.stringify({ time });
            var data2 = JSON.parse(data);
            sessionStorage.setItem('time', data);
          }}
        />
        <br/>
        <br />
        <Button size='large' id="filter-button" onClick={() => { refreshMap(); }}>Filter</Button>
        <Button size='large' id="clear-filter-button" style={{marginLeft: '30px'}} onClick={() => { clear_filter(); }}> Clear filter</Button><br/>
        <p>Or show all hotspots.</p>
        <Button size='large' id="show-button" onClick={() => { showallfeatures(); }}>Show all hotspots</Button>

        <br/>
        <p >Manually add a hotspot</p>
        <Button size='large' id="draw-button" onClick={() => { drawhotspot(); }}>Draw a point</Button>
        <Button size='large' id="save-button" style={{marginLeft: '10px'}} onClick={() => { savenewfeature(); }}>Save</Button>
      </div>
      </div>
      <div class="footer" style={{margin: '0px', padding: '0px', height: '50px', width: '50%'}}>
        <p style={{marginLeft: '700px'}}>Find this app on <a href="https://gitlab.com/k1483/ororatech-technical-challenge.git" target='blank'>gitlab</a> &#169;<a href='https://gitlab.com/akudekar' target='blank'>akudekar</a> </p>
        </div>
    </div>
  );

};
    
export default App;

