import React, { useEffect, useRef } from 'react';
import 'ol/ol.css';
import Map from 'ol/Map';
import View from 'ol/View';
import TileLayer from 'ol/layer/Tile';
import VectorLayer from 'ol/layer/Vector';
import VectorSource from 'ol/source/Vector';
import GeoJSON from 'ol/format/GeoJSON';
import Select from 'ol/interaction/Select';
import OSM from 'ol/source/OSM';

function MapRender() {
  const mapRef = useRef(null);

  useEffect(() => {
    const vectorlayer = new VectorLayer({
        source: new VectorSource({
          format: new GeoJSON(),
          url: 'http://localhost:5000/data',
        }),
        visible: true,
      })

    const select = new Select({
      layers: [vectorlayer]
    });

    const map = new Map({
        layers: [
          new TileLayer({
            source: new OSM()
          }),
          vectorlayer
        ],
        view: new View({
          center: [0, 0],
          zoom: 2
        })
      });

    select.on('select', (event) => {
        console.log(event.target.getFeatures().getArray()[0].getProperties());
      });

    map.addInteraction(select);


    map.on('click', function (e) {
      console.log(e.coordinate);
    })
    return () => {
      map.setTarget(null);
    };
  }, []);
}

export default MapRender;