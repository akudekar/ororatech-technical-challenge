from flask import Flask, jsonify, json, request
import psycopg2
from flask_cors import CORS
from datetime import datetime

app = Flask(__name__)
CORS(app)

@app.route('/data')
def get_data():
    conn = psycopg2.connect(
        host="postgis",
        port="5432",
        database="gis",
        user="postgres",
        password="postgres")
    cur = conn.cursor()
    cur.execute("SELECT id, satellite, radius, tz, st_asgeojson(geom ,4326) FROM public.forest_fires group by id, satellite, radius, tz")
    rows = cur.fetchall()
    features = []
    for row in rows:
        geom = json.loads(row[4])
        feature = {
            'type': 'Feature',
            'geometry': geom,
            'properties': {
                'id': row[0],
                'timestamp': row[3],
                'satellite': row[1],
                'radius': row[2],
            }
        }
        features.append(feature)
    cur.close()
    conn.close()
    feature_collection = {
        'type': 'FeatureCollection',
        'features': features
    }
    return jsonify(feature_collection)


@app.route('/data/filtered')
def filtered():
    try:
        starttime = request.args.get('start_time')
    except:
        starttime = None
    try:
        endtime = request.args.get('end_time')
    except:
        endtime = None
    try:
        satellite = request.args.get('satellite')
    except:
        satellite = None
        
    conn = psycopg2.connect(
        host="postgis",
        port="5432",
        database="gis",
        user="postgres",
        password="postgres")
        
    cur = conn.cursor()
    filteredsql = ""
    
    if satellite == None:
        filteredsql = f"SELECT id, satellite, radius, tz, st_asgeojson(geom,4326) FROM public.forest_fires where tz > '{starttime}' and tz < '{endtime}'"
    elif satellite != None and starttime == None and endtime == None:
        filteredsql = f"SELECT id, satellite, radius, tz, st_asgeojson(geom,4326) FROM public.forest_fires where satellite = '{satellite}'"
    elif satellite != None and starttime != None and endtime != None:
        filteredsql = f"SELECT id, satellite, radius, tz, st_asgeojson(geom,4326) FROM public.forest_fires where tz > '{starttime}' and tz < '{endtime} and satellite = '{satellite}'"
    
    cur.execute(filteredsql)
    rows = cur.fetchall()
    features = []
    for row in rows:
        geom = json.loads(row[4])
        feature = {
            'type': 'Feature',
            'geometry': geom,
            'properties': {
                'id': row[0],
                'timestamp': row[3],
                'satellite': row[1],
                'radius': row[2],
            }
        }
        features.append(feature)
    cur.close()
    conn.close()
    feature_collection = {
        'type': 'FeatureCollection',
        'features': features
    }
    return jsonify(feature_collection)

# Experimenatl feature to filter by satellites, did not implement in the frontend, but is quite easy to do.

@app.route('/data/satellite')
def satelliteList():
    conn = psycopg2.connect(
        host="postgis",
        port="5432",
        database="gis",
        user="postgres",
        password="postgres")
    cur = conn.cursor()
    cur.execute("select distinct satellite from public.forest_fires")
    rows = cur.fetchall()
    satellitelist = []
    for row in rows:
        satellitelist.append(row[0])
    cur.close()
    conn.close()
    return jsonify(satellitelist)

# Get the initial merged layer from the database as a polygon. Any on-the-fly calculations are too slow for the frontend. 
# I would put a trigger function on the table to detect change and redraw the union layer, but I did not have time to implement this.

@app.route('/data/union')
def union():
    conn = psycopg2.connect(
        host="postgis",
        port="5432",
        database="gis",
        user="postgres",
        password="postgres")
    cur = conn.cursor()
    cur.execute("SELECT st_asgeojson(st_union,4326) FROM public.union")
    rows = cur.fetchall()
    features = []
    for row in rows:
        geom = json.loads(row[0])
        feature = {
            'type': 'Feature',
            'geometry': geom,
        }
        features.append(feature)
    cur.close()
    conn.close()
    feature_collection = {
        'type': 'FeatureCollection',
        'features': features
    }
    return jsonify(feature_collection)

# Save manual hotspots to db

@app.route('/data/insert')
def addhotspots():
    lon = request.args.get('lon')
    lat = request.args.get('lat')
    conn = psycopg2.connect(
        host="postgis",
        port="5432",
        database="gis",
        user="postgres",
        password="postgres")
    cur = conn.cursor()
    insertsql = f"insert into public.forest_fires (tz, geom) values ('{datetime.now()}', ST_GeomFromText('POINT({lon} {lat})', 4326))"
    print(insertsql)
    cur.execute(insertsql)
    return {'status': 'success'}

if __name__ == '__main__':
    app.run()